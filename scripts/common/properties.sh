FILES_DIR="$PWD/files"

DOWNLOADS_DIR="$PWD/downloads"
STAGE3_LATEST="latest-stage3-armv6j_hardfp.txt"
PORTAGE_ARCHIVE="portage-latest.tar.xz"
KERNEL_SOURCES_DIR="linux"
FIRMWARE_DIR="firmware"
TOOLS_DIR="tools"

KERNELS_DIR="$PWD/kernels"
KERNEL_DIR="$KERNELS_DIR/linux"
KERNEL_ARCH="arm"
KERNEL_CROSS_COMPILE="armv6j-hardfloat-linux-gnueabi-"

ROOTFSES_DIR="$PWD/rootfses"
ROOTFS_DIR="$ROOTFSES_DIR/rootfs"

INSTALL_KERNEL_DIR=""
INSTALL_ROOTFS_DIR=""
