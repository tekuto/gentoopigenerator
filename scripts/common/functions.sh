function printLog() {
    echo "$@" 1>&2
}

function makeDir() {
    local _PATH="$1"

    if [ -d "$_PATH" ]
    then
        printLog "ディレクトリが既に存在する"

        return 0
    fi

    mkdir -p "$_PATH"
    if [ "$?" -ne 0 ]
    then
        printLog "ディレクトリ作成に失敗"

        return 1
    fi

    return 0
}

function getStage3Latest() {
    local _LATEST_FILE="$1"

    local _LATEST=`egrep -v '^#' $_LATEST_FILE | awk '{ print $1; }'`
    if [ "$_LATEST" = '' ]
    then
        printLog "stage3最新版パスの生成に失敗"

        return 1
    fi

    echo "$_LATEST"

    return 0
}

function isKernelDefconfigured() {
    local _KERNEL_DEFCONFIGURED="$KERNEL_DIR.defconfigured"

    if [ -e "$_KERNEL_DEFCONFIGURED" ]
    then
        return 1
    fi

    return 0
}

function kernelDefconfigured() {
    local _KERNEL_DEFCONFIGURED="$KERNEL_DIR.defconfigured"

    touch "$_KERNEL_DEFCONFIGURED"
}

function makeKernel() {
    make ARCH="$KERNEL_ARCH" CROSS_COMPILE="$KERNEL_CROSS_COMPILE" -C"$DOWNLOADS_DIR/$KERNEL_SOURCES_DIR" O="$KERNEL_DIR" $@
    if [ "$?" -ne 0 ]
    then
        printLog "カーネルmakeに失敗"

        return 1
    fi

    return 0
}
