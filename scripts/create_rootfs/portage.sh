function createPortage() {
    local _PORTAGE_PATH="$DOWNLOADS_DIR/$PORTAGE_ARCHIVE"

    printLog "portageアーカイブ: $_PORTAGE_PATH"

    tar xfJ "$_PORTAGE_PATH" -C "$CREATE_ROOTFS_PORTAGE_DIR"
    if [ "$?" -ne 0 ]
    then
        printLog "portageの展開に失敗"

        return 1
    fi

    return 0
}
