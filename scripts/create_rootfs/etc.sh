function patchToEtc() {
    for _ETC_PATCH in $CREATE_ROOTFS_ETC_PATCHES
    do
        local _ETC_PATCH_PATH="$FILES_DIR/etc/$_ETC_PATCH"

        patch -p1 < "$_ETC_PATCH_PATH"
        if [ "$?" -ne 0 ]
        then
            printLog "${_ETC_PATCH_PATH}のパッチ適用に失敗"

            return 1
        fi
    done
}

function enableToSwclock() {
    rm etc/runlevels/boot/hwclock
    if [ "$?" -ne 0 ]
    then
        printLog "ハードウェアクロックの無効化に失敗"

        return 1
    fi

    ln -s /etc/init.d/swclock etc/runlevels/boot
    if [ "$?" -ne 0 ]
    then
        printLog "ソフトウェアクロックの有効化に失敗"

        return 1
    fi

    return 0
}

function createTimezone() {
    echo "$CREATE_ROOTFS_TIMEZONE" > etc/timezone
    if [ "$?" -ne 0 ]
    then
        printLog "タイムゾーンファイルの生成に失敗"

        return 1
    fi

    return 0
}

function createNet() {
    cp "$FILES_DIR/etc/conf.d/net" etc/conf.d
    if [ "$?" -ne 0 ]
    then
        printLog "ネットワーク設定ファイルのコピーに失敗"

        return 1
    fi

    ln -s net.lo etc/init.d/net.eth0
    if [ "$?" -ne 0 ]
    then
        printLog "ネットワークアダプタの起動スクリプトのシンボリックリンク作成に失敗"

        return 1
    fi

    ln -s /etc/init.d/net.eth0 etc/runlevels/default
    if [ "$?" -ne 0 ]
    then
        printLog "ネットワークアダプタの有効化に失敗"

        return 1
    fi

    return 0
}

function createEtc() {
    patchToEtc
    if [ "$?" -ne 0 ]
    then
        printLog "etcへのパッチ適用に失敗"

        return 1
    fi

    enableToSwclock
    if [ "$?" -ne 0 ]
    then
        printLog "ソフトウェアクロックの有効化に失敗"

        return 1
    fi

    createTimezone
    if [ "$?" -ne 0 ]
    then
        printLog "タイムゾーン設定の生成に失敗"

        return 1
    fi

    createNet
    if [ "$?" -ne 0 ]
    then
        printLog "ネットワーク関係の設定生成に失敗"

        return 1
    fi

    return 0
}
