function createStage3() {
    local _LATEST=`getStage3Latest "$DOWNLOADS_DIR/$STAGE3_LATEST"`
    if [ "$?" -ne 0 ]
    then
        printLog "stage3最新版パスの生成に失敗"

        return 1
    fi

    local _LATEST_NAME=`basename "$_LATEST"`
    if [ "$?" -ne 0 ]
    then
        printLog "stage3のファイル名取得に失敗"

        return 1
    fi

    local _LATEST_PATH="$DOWNLOADS_DIR/$_LATEST_NAME"

    printLog "stage3アーカイブ: $_LATEST_PATH"

    tar xfjp "$_LATEST_PATH"
    if [ "$?" -ne 0 ]
    then
        printLog "stage3の展開に失敗"

        return 1
    fi

    return 0
}
