function createBoot() {
    for _FIRMWARE_FILE in $CREATE_ROOTFS_FIRMWARE_FILES
    do
        local _FIRMWARE_FILE_PATH="$DOWNLOADS_DIR/$FIRMWARE_DIR/boot/$_FIRMWARE_FILE"

        cp "$_FIRMWARE_FILE_PATH" boot
        if [ "$?" -ne 0 ]
        then
            printLog "${_FIRMWARE_FILE_PATH}のコピーに失敗"

            return 1
        fi
    done

    local _CMDLINE_TXT_PATH="$FILES_DIR/boot/cmdline.txt"
    cp "$_CMDLINE_TXT_PATH" boot
    if [ "$?" -ne 0 ]
    then
        printLog "${_CMDLINE_TXT_PATH}のコピーに失敗"

        return 1
    fi

    return 0
}
