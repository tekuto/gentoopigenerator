function createAfterBoot() {
    cp "$FILES_DIR/root/after_boot.sh" root
    if [ "$?" -ne 0 ]
    then
        printLog "起動後の処理のコピーに失敗"

        return 1
    fi

    return 0
}
