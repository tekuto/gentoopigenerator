function downloadGit() {
    local _TARGET="$1"
    local _REPOSITORY="$2"
    local _GIT="$3"
    local _BRANCH="$4"
    local _DIR="$5"

    printLog "ダウンロード元: $_REPOSITORY"

    local _CLONED="$_DIR.cloned"

    if [ -e "$_CLONED" ]
    then
        git -C "$_DIR" pull
        if [ "$?" -ne 0 ]
        then
            printLog "${_TARGET}の更新に失敗"

            return 1
        fi
    else
        if [ -e "$_DIR" ]
        then
            rm -rf "$_DIR"
        fi

        makeDir "$_DIR"
        if [ "$?" -ne 0 ]
        then
            printLog "${_TARGET}ダウンロード先ディレクトリ生成に失敗"

            return 1
        fi

        local _URL="$_REPOSITORY/$_GIT"

        git clone --depth 1 "$_URL" --branch "$_BRANCH" "$_DIR"
        if [ "$?" -ne 0 ]
        then
            printLog "${_TARGET}の取得に失敗"

            rm -r "$_DIR"

            return 1
        fi

        touch "$_CLONED"
    fi

    return 0
}
