function getStage3Latest() {
    local LATEST_FILE="$1"

    local LATEST=`egrep -v '^#' $LATEST_FILE | awk '{ print $1; }'`
    if [ "$LATEST" = '' ]
    then
        printLog "stage3最新版パスの生成に失敗"

        return 1
    fi

    echo "$LATEST"

    return 0
}

function downloadStage3() {
    printLog "ダウンロード元: $GENTOO_MIRROR"

    local LATEST_INFO_URL="$GENTOO_MIRROR/$STAGE3_ROOT/$STAGE3_LATEST"

    printLog "stage3最新版情報: $LATEST_INFO_URL"

    curl -O "$LATEST_INFO_URL"
    if [ "$?" -ne 0 ]
    then
        printLog "stage3最新版情報の取得に失敗"

        return 1
    fi

    local LATEST=`getStage3Latest "$STAGE3_LATEST"`
    if [ "$?" -ne 0 ]
    then
        printLog "stage3最新版パスの生成に失敗"

        return 1
    fi

    local STAGE3_LATEST_CACHE="$STAGE3_LATEST.cache"

    if [ -r "$STAGE3_LATEST_CACHE" ]
    then
        local LATEST_CACHE=`getStage3Latest "$STAGE3_LATEST_CACHE"`

        if [ "$LATEST" = "$LATEST_CACHE" ]
        then
            printLog "stage3最新版ダウンロード済み"

            return 0
        fi
    fi

    local LATEST_URL="$GENTOO_MIRROR/$STAGE3_ROOT/$LATEST"

    printLog "stage3最新版: $LATEST_URL"

    curl -O "$LATEST_URL"
    if [ "$?" -ne 0 ]
    then
        printLog "stage3最新版の取得に失敗"

        return 1
    fi

    cp "$STAGE3_LATEST" "$STAGE3_LATEST_CACHE"

    return 0
}
