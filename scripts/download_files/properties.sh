GENTOO_MIRROR="http://distfiles.gentoo.org"
RASPBERRYPI_REPOSITORY="https://github.com/raspberrypi"

STAGE3_ROOT="releases/arm/autobuilds"

PORTAGE_ROOT="snapshots"
PORTAGE_MD5SUM="$PORTAGE_ARCHIVE.md5sum"

KERNEL_SOURCES_GIT="linux.git"
KERNEL_SOURCES_BRANCH="master"

FIRMWARE_GIT="firmware.git"
FIRMWARE_BRANCH="master"

TOOLS_GIT="tools.git"
TOOLS_BRANCH="master"
