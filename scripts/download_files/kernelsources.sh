function downloadKernelSources() {
    downloadGit "カーネルソース" "$RASPBERRYPI_REPOSITORY" "$KERNEL_SOURCES_GIT" "$KERNEL_SOURCES_BRANCH" "$KERNEL_SOURCES_DIR"

    return "$?"
}
