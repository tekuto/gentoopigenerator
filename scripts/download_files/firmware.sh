function downloadFirmware() {
    downloadGit "ファームウェア" "$RASPBERRYPI_REPOSITORY" "$FIRMWARE_GIT" "$FIRMWARE_BRANCH" "$FIRMWARE_DIR"

    return "$?"
}
