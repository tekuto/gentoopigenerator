function downloadPortage() {
    printLog "ダウンロード元: $GENTOO_MIRROR"

    local PORTAGE_MD5SUM_URL="$GENTOO_MIRROR/$PORTAGE_ROOT/$PORTAGE_MD5SUM"

    printLog "portageのMD5サム: $PORTAGE_MD5SUM_URL"

    curl -O "$PORTAGE_MD5SUM_URL"
    if [ "$?" -ne 0 ]
    then
        printLog "portageのMD5サム取得に失敗"

        return 1
    fi

    local PORTAGE_MD5SUM_CACHE="$PORTAGE_MD5SUM.cache"

    if [ -r "$PORTAGE_MD5SUM_CACHE" ]
    then
        diff "$PORTAGE_MD5SUM" "$PORTAGE_MD5SUM_CACHE"
        if [ "$?" -eq 0 ]
        then
            printLog "portageダウンロード済み"

            return 0
        fi
    fi

    local PORTAGE_ARCHIVE_URL="$GENTOO_MIRROR/$PORTAGE_ROOT/$PORTAGE_ARCHIVE"

    printLog "portage: $PORTAGE_ARCHIVE_URL"

    curl -O "$PORTAGE_ARCHIVE_URL"
    if [ "$?" -ne 0 ]
    then
        printLog "portageのダウンロードに失敗"

        return 1
    fi

    cp "$PORTAGE_MD5SUM" "$PORTAGE_MD5SUM_CACHE"

    return 0
}
