#!/bin/sh

. "scripts/common/properties.sh"
. "scripts/create_rootfs/properties.sh"
for _PROPERTY in $@
do
    . "./$_PROPERTY"
done

. "scripts/common/functions.sh"
. "scripts/create_rootfs/stage3.sh"
. "scripts/create_rootfs/portage.sh"
. "scripts/create_rootfs/boot.sh"
. "scripts/create_rootfs/etc.sh"
. "scripts/create_rootfs/afterboot.sh"

printLog "rootfs生成先: $ROOTFS_DIR"

if [ -e "$ROOTFS_DIR" ]
then
    rm -rf "$ROOTFS_DIR"
fi

makeDir "$ROOTFS_DIR"
if [ "$?" -ne 0 ]
then
    printLog "rootfs生成先ディレクトリ作成に失敗"

    exit 1
fi

cd "$ROOTFS_DIR"

printLog "#### stage3 ####"
createStage3
if [ "$?" -ne 0 ]
then
    printLog "stage3の生成に失敗"

    exit 1
fi

printLog "#### portage ####"
createPortage
if [ "$?" -ne 0 ]
then
    printLog "portageの生成に失敗"

    exit 1
fi

printLog "#### boot ####"
createBoot
if [ "$?" -ne 0 ]
then
    printLog "bootの生成に失敗"

    exit 1
fi

printLog "#### etc ####"
createEtc
if [ "$?" -ne 0 ]
then
    printLog "etcの生成に失敗"

    exit 1
fi

printLog "#### 起動後の処理 ####"
createAfterBoot
if [ "$?" -ne 0 ]
then
    printLog "起動後の処理の生成に失敗"

    exit 1
fi
