#!/bin/sh

. "scripts/common/properties.sh"
for _PROPERTY in $@
do
    . "./$_PROPERTY"
done

. "scripts/common/functions.sh"

printLog "rootfsインストール先: $INSTALL_ROOTFS_DIR"

if [ ! -d "$INSTALL_ROOTFS_DIR" ]
then
    printLog "rootfsインストール先${INSTALL_ROOTFS_DIR}がディレクトリではない"

    exit 1
fi

tar cfp - -C "$ROOTFS_DIR" . | tar xfp - -C "$INSTALL_ROOTFS_DIR"
if [ "$?" -ne 0 ]
then
    printLog "rootfsのインストールに失敗"

    exit 1
fi

echo "#### sync"
sync
sync
sync
