#!/bin/sh

. "scripts/common/properties.sh"
. "scripts/install_kernel/properties.sh"
for _PROPERTY in $@
do
    . "./$_PROPERTY"
done

. "scripts/common/functions.sh"

printLog "カーネルインストール先: $INSTALL_KERNEL_DIR"

if [ ! -d "$INSTALL_KERNEL_DIR" ]
then
    printLog "カーネルインストール先${INSTALL_KERNEL_DIR}がディレクトリではない"

    exit 1
fi

printLog "#### カーネルモジュールインストール"
makeKernel "INSTALL_MOD_PATH=$INSTALL_KERNEL_DIR" modules_install
if [ "$?" -ne 0 ]
then
    printLog "カーネルモジュールのインストールに失敗"

    exit 1
fi

printLog "#### カーネルインストール"
_KERNEL_DIR="$INSTALL_KERNEL_DIR/boot"
if [ ! -e "$_KERNEL_DIR" ]
then
    makeDir "$_KERNEL_DIR"
    if [ "$?" -ne 0 ]
    then
        printLog "カーネルインストール先ディレクトリの生成に失敗"

        exit 1
    fi
fi

"$DOWNLOADS_DIR/$KERNEL_SOURCES_DIR/scripts/mkknlimg" --dtok "$KERNEL_DIR/arch/arm/boot/zImage" "$_KERNEL_DIR/$INSTALL_KERNEL_NAME"
if [ "$?" -ne 0 ]
then
    printLog "カーネルのインストールに失敗"

    exit 1
fi

printLog "#### dtbファイルインストール"
_DTB_DIR="$INSTALL_KERNEL_DIR/boot"
cp "$KERNEL_DIR/arch/arm/boot/dts/"*.dtb "$_DTB_DIR"
if [ "$?" -ne 0 ]
then
    printLog "dtboファイルインストール先ディレクトリの生成に失敗"

    exit 1
fi

printLog "#### dtboファイルインストール"
_DTBO_DIR="$_DTB_DIR/overlays"
if [ ! -e "$_DTBO_DIR" ]
then
    makeDir "$_DTBO_DIR"
    if [ "$?" -ne 0 ]
    then
        printLog "dtboファイルインストール先ディレクトリの生成に失敗"

        exit 1
    fi
fi

cp "$KERNEL_DIR/arch/arm/boot/dts/overlays/"*.dtbo "$_DTBO_DIR"
if [ "$?" -ne 0 ]
then
    printLog "dtboファイルのインストールに失敗"

    exit 1
fi
