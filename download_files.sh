#!/bin/sh

. "scripts/common/properties.sh"
. "scripts/download_files/properties.sh"
for _PROPERTY in $@
do
    . "./$_PROPERTY"
done

. "scripts/common/functions.sh"
. "scripts/download_files/common.sh"
. "scripts/download_files/stage3.sh"
. "scripts/download_files/portage.sh"
. "scripts/download_files/kernelsources.sh"
. "scripts/download_files/firmware.sh"
. "scripts/download_files/tools.sh"

printLog "ダウンロード先: $DOWNLOADS_DIR"

makeDir "$DOWNLOADS_DIR"
if [ "$?" -ne 0 ]
then
    printLog "ダウンロード先ディレクトリ作成に失敗"

    exit 1
fi

cd "$DOWNLOADS_DIR"

printLog "#### stage3最新版 ####"
downloadStage3
if [ "$?" -ne 0 ]
then
    printLog "stage3最新版ダウンロード失敗"

    exit 1
fi

printLog "#### portage ####"
downloadPortage
if [ "$?" -ne 0 ]
then
    printLog "portageダウンロード失敗"

    exit 1
fi

printLog "#### カーネルソース ####"
downloadKernelSources
if [ "$?" -ne 0 ]
then
    printLog "カーネルソースダウンロード失敗"

    exit 1
fi

printLog "#### ファームウェア ####"
downloadFirmware
if [ "$?" -ne 0 ]
then
    printLog "ファームウェアダウンロード失敗"

    exit 1
fi

printLog "#### ツール ####"
downloadTools
if [ "$?" -ne 0 ]
then
    printLog "ツールダウンロード失敗"

    exit 1
fi
