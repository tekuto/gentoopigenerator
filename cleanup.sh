#!/bin/sh

. "scripts/common/properties.sh"
for _PROPERTY in $@
do
    . "./$_PROPERTY"
done

. "scripts/common/functions.sh"

printLog "#### ダウンロードディレクトリ削除 ####"
rm -rf "$DOWNLOADS_DIR"

printLog "#### カーネルディレクトリ削除 ####"
rm -rf "$KERNELS_DIR"

printLog "#### rootfsディレクトリ削除 ####"
rm -rf "$ROOTFSES_DIR"
