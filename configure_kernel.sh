#!/bin/sh

. "scripts/common/properties.sh"
. "scripts/configure_kernel/properties.sh"
for _PROPERTY in $@
do
    . "./$_PROPERTY"
done

. "scripts/common/functions.sh"

printLog "カーネルコンフィグ先: $KERNEL_DIR"

isKernelDefconfigured
if [ "$?" -eq 0 ]
then
    if [ -e "$KERNEL_DIR" ]
    then
        rm -rf "$KERNEL_DIR"
    fi

    makeDir "$KERNEL_DIR"
    if [ "$?" -ne 0 ]
    then
        printLog "カーネルコンフィグ先ディレクトリ作成に失敗"

        exit 1
    fi

    if [ -e "$CONFIGURE_KERNEL_CONFIG" ]
    then
        cp "$CONFIGURE_KERNEL_CONFIG" "$KERNEL_DIR/.config"
        makeKernel "$CONFIGURE_KERNEL_CONFIGMODE"
    else
        makeKernel "$CONFIGURE_KERNEL_DEFCONFIG" "$CONFIGURE_KERNEL_CONFIGMODE"
        if [ "$?" -ne 0 ]
        then
            printLog "初回カーネルコンフィグに失敗"

            exit 1
        fi
    fi

    kernelDefconfigured
else
    makeKernel "$CONFIGURE_KERNEL_CONFIGMODE"
    if [ "$?" -ne 0 ]
    then
        printLog "カーネルコンフィグに失敗"

        exit 1
    fi
fi
