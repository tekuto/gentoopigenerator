#!/bin/sh

. "scripts/common/properties.sh"
. "scripts/build_kernel/properties.sh"
for _PROPERTY in $@
do
    . "./$_PROPERTY"
done

. "scripts/common/functions.sh"

printLog "カーネルビルド先: $KERNEL_DIR"

isKernelDefconfigured
if [ "$?" -eq 0 ]
then
    printLog "カーネルコンフィグが未実行"

    exit 1
fi

makeKernel "-j$BUILD_KERNEL_JOBS"
if [ "$?" -ne 0 ]
then
    printLog "カーネルビルドに失敗"

    exit 1
fi
